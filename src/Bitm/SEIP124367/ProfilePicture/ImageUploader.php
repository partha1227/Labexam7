<?php
namespace App\Bitm\SEIP124367\ProfilePicture;
use App\Bitm\SEIP124367\Message\Message;
use App\Bitm\SEIP124367\Utility\Utility;

class ImageUploader{
    public $id="";
    public $name="";
    public $image_name="";
    public $conn;
    public $deleted_at;


///prepare date
    public function prepare($data=array()){
        if(array_key_exists("name",$data)){
            $this->name=  filter_var($data['name'], FILTER_SANITIZE_STRING);
        }
        if(array_key_exists("image",$data)){
            $this->image_name=  filter_var($data['image'], FILTER_SANITIZE_STRING);
        }
        if(array_key_exists("id",$data)){
            $this->id= $data['id'];
        }

        return $this;

    }
///prepare connection
    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","labexam7") or die("Database connection failed");
    }


////insert data into database
    public function store(){
        $query="INSERT INTO  profilepicture ( `name`, `images`) VALUES ('{$this->name}', '{$this->image_name}')";
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has benn stored successfully");
            Utility::redirect('index.php');

        }else{
            Message::message("Data has not been stored successfully");
            Utility::redirect('index.php');
        }
    }


    public function index(){
        $_allInfo= array();
        $query="SELECT * FROM profilepicture WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
       while($row= mysqli_fetch_assoc($result)){
            $_allInfo[]=$row;
        }

        return $_allInfo;
    }

    public function view(){
        $query="SELECT * FROM `profilepicture` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }


///////make active
    public function makeactive(){
        $query="SELECT * FROM `profilepicture` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        if ($result){
        $row= mysqli_fetch_assoc($result);
            return $row;
            Utility::redirect('index.php');
        }else{
            $query="SELECT * FROM `profilepicture` WHERE status='1' LIMIT 1 ";
            $result= mysqli_query($this->conn,$query);
            $row= mysqli_fetch_assoc($result);
            return $row;
            Utility::redirect('index.php');
        }


    }

//////make deactive
public function deactive(){
    $query="SELECT status FROM profilepicture WHERE id=".$this->id;
    $result= mysqli_query($this->conn,$query);
    $row= mysqli_fetch_assoc($result);

        if ($row['status']==1)
        {
            $query = "UPDATE `profilepicture` SET `status` = 0 WHERE `id`=" . $this->id;
        } else {
            $query = "UPDATE `profilepicture` SET `status` = 1 WHERE `id`=" . $this->id;
        }
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);

    if($row){
        Utility::redirect('index.php');
    }else{
        Utility::redirect('index.php');
    }
}


/////update 
    public function update(){
        $profilePicture= new ImageUploader();
        $singleInfo= $profilePicture->prepare($_GET)->view();
        if ($singleInfo['images']== $this->image_name){
            $query="UPDATE `profilepicture` SET `name` = '{$this->name}'
WHERE `profilepicture`.`id` = ".$this->id;
            $result= mysqli_query($this->conn,$query);}
        else{
            $query="UPDATE `profilepicture` SET `name` = '{$this->name}', `images` = '{$this->image_name}'
WHERE `profilepicture`.`id` = ".$this->id;
            $result= mysqli_query($this->conn,$query);
        }
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Updated successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated  successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

/////delete
    public function delete(){
        $query="DELETE FROM `profilepicture` WHERE `profilepicture`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }


    /////////trash temporary
    public function trash(){
        $this->deleted_at=time();
        $query="UPDATE `profilepicture` SET `deleted_at` = '".$this->deleted_at."' WHERE `profilepicture`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Trashed!</strong> Data has been trashed successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been trashed successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

//////////trash temporary list
    public function trashed(){
        $_trashedPict= array();
        $query="SELECT * FROM  `profilepicture` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $_trashedPict[]=$row;
        }
        return $_trashedPict;
    }
//////////recover single
    public function recover(){
        $query="UPDATE `profilepicture` SET `deleted_at` = NULL  WHERE `profilepicture`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
          <strong>Recovered!</strong> Data has been recovered successfully.
        </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
          <strong>Error!</strong> Data has not been recovered successfully.
            </div>");
            Utility::redirect('index.php');

        }

    }

}