<?php
session_start();
include_once('../../vendor/autoload.php');
use App\Bitm\SEIP124367\ProfilePicture\ImageUploader;
use App\Bitm\SEIP124367\Utility\Utility;
use App\Bitm\SEIP124367\Message\Message;


$profilePicture= new ImageUploader();
$allInfo=$profilePicture->index();
//Utility::d($allInfo);


$picture= new ImageUploader();
$singleData=$picture->prepare($_GET)->makeactive();


//$pictured= new ImageUploader();
//$sigleDeactive=$pictured->prepare($_GET)->deactive();

//$profilepicture= new ImageUploader(); //orignal
//$singleData=$profilepicture->prepare($_GET)->makeactive();
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <!--  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
</head>
<body>

<div class="container">
    <h2>Welcome to User Profile Picture</h2>
   
    <a href="create.php" class="btn btn-info" role="button">Add User Profile Picture</a>
    <a href="trashed_view.php" class="btn btn-primary" role="button">Trashed List</a><br><br>
    <br>
    <div id="message">

        <?php if((array_key_exists('message',$_SESSION)&& !empty($_SESSION['message']))){
            echo Message::message();
        }?>
    </div>

   <!----make active--->


    <div class="container">

        <h2> On Active  </h2>

        <ul class="list-group">
            <li class="list-group-item">ID: <?php echo $singleData['id']?> </li>
            <li class="list-group-item">Name: <?php echo $singleData['name'] ?> </li>
            <li> <img src="../../Resources/Images/<?php echo $singleData['images']?>" alt="image" height="100px" width="100px"></li>

        </ul>
    </div>

    
    <!----end active -->








    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>SL#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Profile Picture</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach($allInfo as $info){
                $sl++;
                ?>
                <tr>

                    <td><?php echo $sl; ?></td>
                    <td><?php echo $info['id'] // for object: $book->id ; ?></td>
                    <td><?php echo $info['name'] // for object: $book->title; ?></td>
                    <td><img src="../../Resources/Images/<?php echo $info['images']?>" alt="image" height="100px" width="100px"></td>
                    <td>
                        <?php
                        if ($info['status']==0) {
                        ?>

                            <a href="deactive.php?id=<?php echo $info['id']?>" class="btn btn-info  btn-xs" role="button">Make
                                Active</a>
                        <?php
                        } else {
                            ?>
                            <a href="deactive.php?id=<?php echo $info['id']?>" class="btn btn-success  btn-xs" role="button">
                                Actived</a>
                        <?php
                        }
                        ?>

                        <a href="deactive.php?id=<?php echo $info['id']?>" class="btn btn-danger  btn-xs" role="button">Make Deactive</a>
                        <a href="view.php?id=<?php echo $info['id']?>" class="btn btn-info  btn-xs" role="button">View</a>
                        <a href="edit.php?id=<?php echo $info['id']?>" class="btn btn-primary  btn-xs" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo$info['id']?>" class="btn btn-danger  btn-xs" role="button">Delete</a>
                        <a href="trash.php?id=<?php echo $info['id']?>" class="btn btn-info  btn-xs" role="button">Trash</a>
                    </td>


                </tr>
            <?php } ?>


            </tbody>
        </table>
    </div>
</div>
<script>
    $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>
