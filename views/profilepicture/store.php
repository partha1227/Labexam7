<?php
include_once ('../../vendor/autoload.php');

use App\Bitm\SEIP124367\ProfilePicture\ImageUploader;
use App\Bitm\SEIP124367\Utility\Utility;
use App\Bitm\SEIP124367\Message\Message;


if((isset($_FILES['image'])) && (!empty($_FILES['image']['name']))){
    $imageName= time(). $_FILES['image']['name'];
    $temporary_location= $_FILES['image']['tmp_name'];


    $imgFile = $_FILES['image']['name'];
    $tmp_dir = $_FILES['image']['tmp_name'];
    $imgSize = $_FILES['image']['size'];
    $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));

    $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
    $userpic = rand(1000, 1000000) . "." . $imgExt;
    if (in_array($imgExt, $valid_extensions)) {
        if ($imgSize < 5000000) {   ///check file size 5MB


            move_uploaded_file($temporary_location, '../../Resources/Images/' . $imageName);
            $_POST['image'] = $imageName;

        } else {
            $errMSG = "Sorry, your file is too large it should be less then 5MB";
        }
    }else{
        $errMSG = "Sorry, your file is not a valid extensions";
    }
}
$profilePicture= new ImageUploader();
$profilePicture->prepare($_POST)->store();

//Utility::d($_POST);
