<?php
include_once ('../../vendor/autoload.php');
use App\Bitm\SEIP124367\ProfilePicture\ImageUploader;
use App\Bitm\SEIP124367\Message\Message;
use App\Bitm\SEIP124367\Utility\Utility;


$profilePicture= new ImageUploader();
$singleInfo= $profilePicture->prepare($_POST)->view();

if ((isset($_FILES['image'])) && (!empty($_FILES['image']['name']))) {

    $username = $_POST['name'];// user name
    $imgFile = $_FILES['image']['name'];
    $tmp_dir = $_FILES['image']['tmp_name'];
    $imgSize = $_FILES['image']['size'];




    $uid=$_POST['id'];
    
    $imageName = time() . $_FILES['image']['name'];
    $temporary_location = $_FILES['image']['tmp_name'];
    $mm=$_SERVER['DOCUMENT_ROOT']."Labexam7/Resources/Images/";
    if ($imgFile) {   

        $imgExt = strtolower(pathinfo($imgFile, PATHINFO_EXTENSION)); 
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); 
        $userpic = rand(1000, 1000000) . "." . $imgExt;
        if (in_array($imgExt, $valid_extensions)) {
            if ($imgSize < 5000000) {
                unlink($mm . $singleInfo['images']);
                move_uploaded_file($temporary_location, '../../Resources/Images/' . $imageName);
            } else {
                $errMSG = "Sorry, your file is too large it should be less then 5MB";
            }
        } else {
            $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        }
    }

    $_POST['image'] = $imageName;


}

$profilePicture = new ImageUploader();
$profilePicture->prepare($_POST)->update();

