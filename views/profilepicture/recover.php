<?php
include_once ('../../vendor/autoload.php');
use App\Bitm\SEIP124367\ProfilePicture\ImageUploader;
use App\Bitm\SEIP124367\Message\Message;
use App\Bitm\SEIP124367\Utility\Utility;

$profilepicture = new ImageUploader();
$profilepicture->prepare($_GET)->recover();