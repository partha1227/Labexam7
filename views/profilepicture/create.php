
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile Picture</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<div class="container">
    <a href="index.php" class="btn btn-info" role="button">Main Page</a>
    <h2>Create Profile Picture</h2>
    <form role="form" action="store.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Name:</label>
            <input type="text" class="form-control"  placeholder="Enter your name" name="name">
        </div>
        <div class="form-group">
            <label for="pwd">Upload file:</label>

            <input type="file"  onchange="readURL(this);" class="form-control" name="image">
            <img id="blah" src="../../Resources/Images/no_avatar.png" alt="your image" />
        </div>

        <input type="submit" class="btn btn-default" value="Submit">
    </form>
</div>
</body>
</html>


