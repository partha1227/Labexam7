<?php

include_once('../../vendor/autoload.php');
use App\Bitm\SEIP124367\ProfilePicture\ImageUploader;
use App\Bitm\SEIP124367\Utility\Utility;
use App\Bitm\SEIP124367\Message\Message;

$picture= new ImageUploader();
$singleData=$picture->prepare($_GET)->view();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>User Profile Information </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <a href="index.php" class="btn btn-info" role="button">Main Page Profile Picture</a>
    <h2> View Profile Picture  </h2>

    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleData['id']?> </li>
        <li class="list-group-item">Name: <?php echo $singleData['name'] ?> </li>
        <li> <img src="../../Resources/Images/<?php echo $singleData['images']?>" alt="image" height="100px" width="100px"></li>

    </ul>
</div>

</body>
</html>



